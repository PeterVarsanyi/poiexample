import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;

public class App {

    /**
     * XSSF
     * XWPF -> docx
     *
     * @param args
     */
    public static void main(String[] args) {
        openDocument();
    }

    private static void openDocument() {
        DocxHandler handler = new DocxHandler();
        try {
            XWPFDocument doc = handler.open("/home/peter/Letöltések/Questions01.docx");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
