import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;

import java.io.*;
import java.util.List;

/**
 * Egy DOCX file kezelését lehetővé tevő osztály.
 * EGY létrehozott vagy EGY megnyitott állományt kezel. Egyszerre csak egyet!
 * A munka végeztével a <code>close</code> metódussal zárja az nyitott dolgokat.
 *
 * https://www.tutorialspoint.com/apache_poi_word/apache_poi_word_document.htm
 *
 * @author peter
 */
public class DocxHandler {

    /**
     * Ez a kezelt dokuemtum.
     */
    protected XWPFDocument document;

    /**
     * Ha nem új, akkor ez az a file.
     * A file neve teljes elérési úttal.
     */
    protected String targetFilename;

    public XWPFDocument getDocument() {
        return document;
    }

    public String getTargetFilename() {
        return targetFilename;
    }

    /**
     * Új, üres dokumentum létrehozása.
     *
     * @return A létrejött dokumentum.
     */
    public XWPFDocument create() {
        this.document = new XWPFDocument();
        return this.document;
    }

    /**
     * Egy létező dokumentum megnyitása.
     *
     * @param filenameWithFullPath Az állomány neve teljes elérési úttal.
     * @return A megnyitott állomány.
     * @throws IOException Bármi hiba volt a megnyitás során, akkor ilyen excpetion-t dob.
     */
    public XWPFDocument open(String filenameWithFullPath) throws IOException {
        File file = new File(filenameWithFullPath);
        return this.open(file);
    }

    /**
     * Egy létező dokumentum megnyitása.
     *
     * @param file A megnyitandó állomány.
     * @return A megnyitott állomány.
     * @throws IOException Bármi hiba volt a megnyitás során, akkor ilyen excpetion-t dob.
     */
    public XWPFDocument open(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        this.document = new XWPFDocument(inputStream);
        inputStream.close();
        this.targetFilename = file.getAbsolutePath();
        return this.document;
    }

    /**
     * A dolgok lezárása.
     * Nem kötelező meghívni - egyenlőre.
     * Az osztályváltozók <code>null</code>-ozása.
     */
    public void close() {
        this.document = null;
        this.targetFilename = null;
    }

    /**
     * Korábban megnyitott állomány mentés.
     * Rámenti az eredetire.
     *
     * @throws IOException Ha bármi hiba volt vagy sikertelen volt a mentés, akkor ilyen exception-t dob.
     * @throws IllegalArgumentException Ha nincs beállított filenév. (Ha üres a <code>targetFilename</code> osztályválotzó.)
     */
    public void save() throws IOException {
        this.save(this.targetFilename);
    }

    /**
     * Korábban megnyitott állomány mentése PDF-ként.
     * Ha már létezett, rámenti.
     * <p>
     * Ha ilyen hiba van:
     * Not all annotations could be added to the document (the document doesn't have enough pages)
     * akkor: a fejben (lábban) nem lehet (url) link! Ki kell törölni és jó lesz!
     * </p>
     *
     * @throws IOException              Ha bármi hiba volt vagy sikertelen volt a mentés, akkor ilyen exception-t dob.
     * @throws IllegalArgumentException Ha nincs beállított filenév. (Ha üres a <code>targetFilename</code> osztályválotzó.)
     */
    public void saveAsPdf() throws IOException {
        this.saveAsPdf(this.targetFilename);
    }

    /**
     * Dokmentum mentése a megadott helyre, névvel.
     *
     * @param filenameWithFullPath A mentés helye és az állomány neve.
     * @throws IOException Ha bármi hiba volt vagy sikertelen volt a mentés, akkor ilyen exception-t dob.
     * @throws IllegalArgumentException Ha üres (vagy <code>null</code>) a paraméter.
     */
    public void save(String filenameWithFullPath) throws IOException {
        if (filenameWithFullPath == null || filenameWithFullPath.trim().isEmpty()) {
            throw new java.lang.IllegalArgumentException("Nincs megadva az állomány elérési útja és neve!");
        }
        FileOutputStream out = new FileOutputStream(filenameWithFullPath);
        this.document.write(out);
        out.close();
        this.targetFilename = filenameWithFullPath;
    }

    /**
     * Dokmentum mentése a megadott helyre, névvel, PDF-ként.
     * Ha a kiterjesztése ".docx", akkor átalakítja ".pdf"-re.
     * <p>
     * https://github.com/opensagres/xdocreport/wiki/XWPFConverterPDFViaIText
     * </p>
     * <p>
     * Ha ilyen hiba van:
     * Not all annotations could be added to the document (the document doesn't have enough pages)
     * akkor: a fejben (lábban) nem lehet (url) link! Ki kell törölni és jó lesz!
     * </p>
     *
     * @param filenameWithFullPath A mentés helye és az állomány neve.
     * @throws IOException              Ha bármi hiba volt vagy sikertelen volt a mentés, akkor ilyen exception-t dob.
     * @throws IllegalArgumentException Ha üres (vagy <code>null</code>) a paraméter.
     */
    public void saveAsPdf(String filenameWithFullPath) throws IOException {
        if (filenameWithFullPath == null || filenameWithFullPath.trim().isEmpty()) {
            throw new java.lang.IllegalArgumentException("Nincs megadva az állomány elérési útja és neve!");
        }

        // pdf kiterjesztés, ha kell
        if (filenameWithFullPath.toLowerCase().endsWith(".docx")) {
            filenameWithFullPath = filenameWithFullPath.replace(".docx", ".pdf");
        }

        // pdf-esítés
        FileOutputStream out = new FileOutputStream(filenameWithFullPath);
        out.write(this.convertToPdf());
        out.close();
        this.targetFilename = filenameWithFullPath;
    }

    /**
     * A dokumentum PDF-é konvertálása.
     * A PDF tartalmat <code>byte</code> tömbként adja vissza.
     * <p>
     * Ha ilyen hiba van:
     * Not all annotations could be added to the document (the document doesn't have enough pages)
     * akkor: a fejben (lábban) nem lehet (url) link! Ki kell törölni és jó lesz!
     * </p>
     *
     * @return A PDF dokumentum <code>byte</code> tömbként.
     * @throws IOException Bármi hiba volt a konvertálás során, az exceptionként jön vissza.
     */
    public byte[] convertToPdf() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        this.convertToPdf(out);
        out.close();
        return out.toByteArray();
    }

    /**
     * Ez egy belső használatú metódus.
     * Ez - ebben van - a PDF konvertálás.
     * Ezt másik metódusok hívják.
     * <p>
     * Ha ilyen hiba van:
     * Not all annotations could be added to the document (the document doesn't have enough pages)
     * akkor: a fejben (lábban) nem lehet (url) link! Ki kell törölni és jó lesz!
     * </p>
     *
     * @param out Az a kimeneti byte stream, amibe az adattartalom kerül.
     * @throws IOException Bármi hiba volt, exceptionként jön vissza.
     */
    private void convertToPdf(ByteArrayOutputStream out) throws IOException {
        PdfOptions options = PdfOptions.create();
        PdfConverter.getInstance().convert(this.document, out, options);
    }

    /**
     * Egy dokumentum létrehozása byte tömb adatokból.
     *
     * @param documentData A docx dokumentum byte tömbként.
     */
    public XWPFDocument create(byte[] documentData) throws IOException {
        InputStream inputStream = new ByteInputStream(documentData, documentData.length);
        this.document = new XWPFDocument(inputStream);
        inputStream.close();
        return this.document;
    }

    /**
     * A dokumentum oldal fejlécét adja vissza.
     * Ha nincs beállítva fejléc, akkor <code>null</code>.
     *
     * @return A (default) fejléc vagy null.
     */
    public XWPFHeader getHeader() {
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(this.document);
        return policy.getDefaultHeader();
    }

    /**
     * A dokumentum oldal láb részét adja vissza.
     * Ha nincs beállítva láb, akkor <code>null</code>.
     *
     * @return A (default) lábléc vagy null.
     */
    public XWPFFooter getFooter() {
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(this.document);
        return policy.getDefaultFooter();
    }

    /**
     * A dokumentum megadott sorszámú táblázatát adja vissza.
     * Ha kisebb, mint 0 vagy nagyobb, mint az elérhető táblázatok száma,
     * akkor <code>null</code>-t ad vissza.
     *
     * @param tableIndex A táblázat index-e. Egész szám.
     * @return A táblázat vagy null.
     */
    public XWPFTable getTable(int tableIndex) {
        return this.document.getTables().size() <= tableIndex || 0 > tableIndex ? null : this.document.getTables().get(tableIndex);
    }

    /**
     * Megadott tábla összes sorának törlése.
     * Csak a legelső (nulladik indexű sort hagyja meg).
     *
     * @param table A megadott tábla. Ha null, akkor nem csinál semmit.
     */
    public void tableClear(XWPFTable table) {
        if (table != null) {
            for (int i = table.getRows().size(); i > 0; i--) {
                table.removeRow(i);
            }
        }
    }

    /**
     * Megadott tábla összes sorának törlése.
     * Csak a legelső (nulladik indexű sort hagyja meg).
     *
     * @param tableIndex A megadott tábla index-e.
     */
    public void tableClear(int tableIndex) {
        this.tableClear(this.getTable(tableIndex));
    }

    /**
     * Egyszerű, létező táblázat feltöltése adatokkal.
     * Nincsenek benne cella összevonások.
     * Felsorolás - lista szerű - táblázat!
     *
     * @param tableIndex A táblázat index-e. Ha nincs ilyen, nem csinál semmit.
     * @param tableData  Az adatok <code>String</code>-ként. Ez egy <code>String</code> lista a listában. Két dimenziós lista.
     *                   Fontos, hogy annyi legyen a második dimenzió mérete, ahány oszlop van. Az üreset is be kell
     *                   tenni üres (és nem <code>null</code>!!!) <code>String</code>-ként!
     */
    public void tablePushData(int tableIndex, List<List<String>> tableData) {
        XWPFTable table = this.getTable(tableIndex);
        if (table != null && tableData != null) {
            int tableRowIndex = 1;
            for (List<String> rowData : tableData) {
                if (rowData != null) { // hátha valaki üres sort akar beletenni, ezért ez nem kell: && !rowData.isEmpty()) {
                    XWPFTableRow tableRow = table.insertNewTableRow(tableRowIndex++);
                    for (String cellData : rowData) {
                        XWPFTableCell cell = tableRow.createCell();
                        XWPFRun run = cell.getParagraphs().get(0).createRun();
                        run.setText(cellData);
                    }
                }
            }
        }
    }


    public void insertPicture(File pictureFile) throws IOException, InvalidFormatException {
        long fileSize = pictureFile.length();
        InputStream inputStream = new FileInputStream(pictureFile);
//        byte[] allBytes = new byte[(int) fileSize];
//        inputStream.read(allBytes);
        XWPFParagraph p = this.document.createParagraph();
        XWPFRun r = p.createRun();
        r.addPicture(inputStream, this.getPictureFormat(pictureFile.getName()), "kep1.jpg", Units.toEMU(20), Units.toEMU(20));
        inputStream.close();
//        this.insertPicture(allBytes, this.getPictureFormat(pictureFile.getName()));
    }

//    public void insertPicture(byte[] imageContent, int pictureFormat) throws InvalidFormatException {
//        XWPFParagraph p = this.document.createParagraph();
//        XWPFRun r = p.createRun();
//        r.addPictureData(imageContent, pictureFormat,
//    }

    public int getPictureFormat(String imgFile) {
        int format;
        if (imgFile.endsWith(".emf"))
            format = XWPFDocument.PICTURE_TYPE_EMF;
        else if (imgFile.endsWith(".wmf"))
            format = XWPFDocument.PICTURE_TYPE_WMF;
        else if (imgFile.endsWith(".pict"))
            format = XWPFDocument.PICTURE_TYPE_PICT;
        else if (imgFile.endsWith(".jpeg") || imgFile.endsWith(".jpg"))
            format = XWPFDocument.PICTURE_TYPE_JPEG;
        else if (imgFile.endsWith(".png"))
            format = XWPFDocument.PICTURE_TYPE_PNG;
        else if (imgFile.endsWith(".dib"))
            format = XWPFDocument.PICTURE_TYPE_DIB;
        else if (imgFile.endsWith(".gif"))
            format = XWPFDocument.PICTURE_TYPE_GIF;
        else if (imgFile.endsWith(".tiff"))
            format = XWPFDocument.PICTURE_TYPE_TIFF;
        else if (imgFile.endsWith(".eps"))
            format = XWPFDocument.PICTURE_TYPE_EPS;
        else if (imgFile.endsWith(".bmp"))
            format = XWPFDocument.PICTURE_TYPE_BMP;
        else if (imgFile.endsWith(".wpg"))
            format = XWPFDocument.PICTURE_TYPE_WPG;
        else {
            return 0;
        }
        return format;
    }

}
