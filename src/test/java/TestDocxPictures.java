import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;

/**
 * Képekkel kapcsolatos tesztek.
 *
 * @author peter
 */
public class TestDocxPictures extends TestDocxBase {

    private final String inputTestPictureDoc1 = inputDir + "poi_test_table1.docx";

    @org.testng.annotations.Test
    public void testInsertPicture() throws IOException, InvalidFormatException {
        String pictureName = "pic_1_645x852.jpg";
        File img = new File(inputDir + pictureName);

        XWPFDocument doc = docxHandler.open(this.inputTestPictureDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testInsertPicture: Nem nyitható meg a dokumentum! " + this.inputTestPictureDoc1);
        docxHandler.insertPicture(img);
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc1.pdf");
    }

}
