import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Alap teszt osztály.
 * Absztract.
 * Ebből érdemes származtatni a teszt osztályokat.
 * Példányosítja minden teszt előtt a <code>docxHandler</code> osztályváltozót.
 * Minden osztályváltozó <code>protected</code>, azért hogy az származtatott osztályokban is
 * el lehessen érni őket.
 * <p>
 * Input és output directory: kell a végére a "/" jel!
 * Alap teszt állományok.
 * A <code>docxHandler</code> osztályváltozó, mellyek elérhetjük a tesztelendő metódusokat.
 * </p>
 */
public abstract class TestDocxBase {

    protected final String inputDir = "/home/peter/IdeaProjects/POIExample01/ProjectFiles/TestDocuments/";
    protected final String oututDir = "/home/peter/Letöltések/";
    protected final String inputFilenameWithPath = inputDir + "poi_test_table1.docx";
    protected final String outputFilenameWithPath = oututDir + "poi_test_table1_POI.docx";

    protected DocxHandler docxHandler = null;

    @BeforeTest
    protected void beforeTest() {
        if (this.docxHandler == null) {
            this.docxHandler = new DocxHandler();
        }
    }

    @AfterTest
    protected void afterTest() {
        if (this.docxHandler != null) {
            this.docxHandler.close();
            this.docxHandler = null;
        }
    }

}
