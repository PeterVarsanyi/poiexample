import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.testng.Assert;

import java.io.IOException;

/**
 * A PDF konvertálás próba.
 * <p>
 * Ha ilyen hiba van:
 * Not all annotations could be added to the document (the document doesn't have enough pages)
 * akkor: a fejben (lábban) nem lehet (url) link! Ki kell törölni és jó lesz!
 * </p>
 */
public class TestConvertPdf extends TestDocxBase {

    //private final String inputTestDoc1 = inputDir + "csapatlista.docx";
    private final String inputTestDoc1 = "c:\\Users\\pvarsanyi\\IdeaProjects\\poiexample\\ProjectFiles\\TestDocuments\\csapatlista.docx";

    @org.testng.annotations.Test
    public void testOnePdfConvert() throws IOException {
        XWPFDocument doc = docxHandler.open(this.inputTestDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testOnePdfConvert: Nem nyitható meg a dokumentum! " + this.inputTestDoc1);
        docxHandler.saveAsPdf("pdf1.pdf");
    }


}
