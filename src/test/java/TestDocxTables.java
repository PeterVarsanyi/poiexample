import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Táblázatos műveletekkel kapcsolatos tesztek.
 *
 * @author peter
 */
public class TestDocxTables extends TestDocxBase {

    private final String inputTestTableDoc1 = inputDir + "poi_test_table1.docx";
    private final String inputTestTableDoc2 = inputDir + "poi_test_table2.docx";

    @org.testng.annotations.Test
    public void testTableList1() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testTableList1: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc1);
        Assert.assertTrue(docxHandler.getDocument().getTables().size() == 1, "testTableList1: nem egy tábla található a dokumentumban!" + this.inputTestTableDoc1);
    }

    @org.testng.annotations.Test
    public void testTableList2() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc2);
        Assert.assertNotNull(docxHandler.getDocument(), "testTableList2: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc2);
        Assert.assertTrue(docxHandler.getDocument().getTables().size() == 2, "testTableList1: nem kettő tábla található a dokumentumban!" + this.inputTestTableDoc2);
    }

    @org.testng.annotations.Test
    public void testTable1ToPdf() throws IOException {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testTable1ToPdf: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc1);
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc1.pdf");
    }

    @org.testng.annotations.Test
    public void testTable2ToPdf() throws IOException {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc2);
        Assert.assertNotNull(docxHandler.getDocument(), "testTable2ToPdf: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc2);
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc2.pdf");
    }

    @org.testng.annotations.Test
    public void testTable1Clear1() throws IOException {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testTable1Clear1: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc1);
        docxHandler.tableClear(0);
        Assert.assertTrue(docxHandler.getTable(0).getRows().size() == 1, "testTable1Clear1: Nem egy a táblazat sorok száma! ");
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc1.pdf");
    }

    @org.testng.annotations.Test
    public void testTable1Clear2() throws IOException {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testTable1Clear2: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc1);
        docxHandler.tableClear(docxHandler.getTable(0));
        Assert.assertTrue(docxHandler.getTable(0).getRows().size() == 1, "testTable1Clear2: Nem egy a táblazat sorok száma! ");
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc1.pdf");
    }

    @org.testng.annotations.Test
    public void testTable1AddRows() throws IOException {
        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testTable1AddRows: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc1);
        docxHandler.tableClear(docxHandler.getTable(0));
        Assert.assertTrue(docxHandler.getTable(0).getRows().size() == 1, "testTable1AddRows: Nem egy a táblazat sorok száma! ");

        List<List<String>> data = new ArrayList<>();

        List<String> dataRow = new ArrayList<>();
        dataRow.add("1");
        dataRow.add("Kati");
        data.add(dataRow);

        dataRow = new ArrayList<>();
        dataRow.add("2");
        dataRow.add("Józsi");
        data.add(dataRow);

        dataRow = new ArrayList<>();
        dataRow.add("3");
        dataRow.add("árvíztűrőtükörfúrógép");
        data.add(dataRow);

        dataRow = new ArrayList<>();
        dataRow.add("4");
        dataRow.add("ÁRVÍZTŰRŐTÜKÖRFÚRÓGÉP");
        data.add(dataRow);

        docxHandler.tablePushData(0, data);
        Assert.assertTrue(docxHandler.getTable(0).getRows().size() != 1, "testTable1AddRows: Sajnos egy a táblazat sorok száma! ");
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc1.pdf");
    }

    @org.testng.annotations.Test
    public void testInsertPicture() throws IOException, InvalidFormatException {
        String pictureName = "pic_1_645x852.jpg";
        File img = new File(inputDir + pictureName);

        XWPFDocument doc = docxHandler.open(this.inputTestTableDoc1);
        Assert.assertNotNull(docxHandler.getDocument(), "testInsertPicture: Nem nyitható meg a dokumentum! " + this.inputTestTableDoc1);
        docxHandler.insertPicture(img);
        docxHandler.saveAsPdf(oututDir + "inputTestTableDoc1.pdf");
    }

}
