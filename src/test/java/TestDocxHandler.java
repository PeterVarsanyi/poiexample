import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.testng.Assert;

import java.io.File;

/**
 * A <code>DocxHandler</code> alapszolgáltatásainak tesztelése.
 * Mentés, megnyitás, mentés PDF-ként, stb.
 *
 * @author peter
 */
public class TestDocxHandler extends TestDocxBase {

    @org.testng.annotations.Test
    public void testCreate() throws Exception {
        docxHandler.create();
        Assert.assertNotNull(docxHandler.getDocument(), "testCreate: Nem jött létre dokumentum!");
    }

    @org.testng.annotations.Test
    public void testOpen() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testOpen: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
    }

    @org.testng.annotations.Test
    public void testOpen1() throws Exception {
        XWPFDocument doc = docxHandler.open(new File(this.inputFilenameWithPath));
        Assert.assertNotNull(docxHandler.getDocument(), "testOpen1: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
    }

    @org.testng.annotations.Test
    public void testSave() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testSave: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
        docxHandler.save(); // ha nem volt exception, akkor ok.
    }

    @org.testng.annotations.Test
    public void testSave1() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testSave1: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
        docxHandler.save(this.outputFilenameWithPath); // ha nem volt exception, akkor ok.
    }

    @org.testng.annotations.Test(expectedExceptions = IllegalArgumentException.class)
    public void testSave2() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testSave2: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
        docxHandler.save(null);
    }

    @org.testng.annotations.Test()
    public void testSavePdf() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testSavePdf: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
        docxHandler.saveAsPdf(this.outputFilenameWithPath);
    }

    @org.testng.annotations.Test
    public void testGetHeader() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testGetHeader: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getHeader(), "testGetHeader: Nincs header!");
    }

    @org.testng.annotations.Test
    public void testGetFooter() throws Exception {
        XWPFDocument doc = docxHandler.open(this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getDocument(), "testGetFooter: Nem nyitható meg a dokumentum! " + this.inputFilenameWithPath);
        Assert.assertNotNull(docxHandler.getFooter(), "testGetFooter: Nincs header!");
    }

}