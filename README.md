# POI Example #

A POI felhasználására egy kis példa program, tesztekkel.

### Célok ###

* DOCX-ek kezelése, egyszerre egy darab.
* Szöveg keres-csere az egész dokumentumban, fejléc és láblécben is. A formátumot megtartva.
* Meglévő táblázatok feltöltése adatokkkal.
* Kép beszúrása megadott helyre.
* A DOCX-ek kimentése (PDF és DOCX formátumban).
* A dokumentum megnyitható állomány és byte tömb megadásával.
* A mentés: állományt és képes visszaadni az (DOCX vagy PDF) állományt byte tömbként.

